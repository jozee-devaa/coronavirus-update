package com.jozzee.android.coronavirusupdate.datasource.spread

import com.google.gson.annotations.SerializedName
import com.jozzee.android.core.datetime.dateTimeFormat
import com.jozzee.android.core.datetime.toDate
import com.jozzee.android.coronavirusupdate.utils.DISPLAY_DATE_TIME_FORMAT

data class ConfirmedCountriesResult(@SerializedName("features") var confirmedCountries: List<ConfirmedCountry>? = null)

data class ConfirmedCountry(@SerializedName("attributes") var attributes: ConfirmedCountryAttribute? = null)

data class ConfirmedCountryAttribute(
        @SerializedName("OBJECTID") var objectId: Int = 0,
        @SerializedName("Country_Region") var name: String = "",
        @SerializedName("Last_Update") var lastUpdate: Long = 0,
        @SerializedName("Lat") var latitude: Double = 0.0,
        @SerializedName("Long_") var longitude: Double = 0.0,
        @SerializedName("Confirmed") var confirmed: Int = 0,
        @SerializedName("Deaths") var deaths: Int = 0,
        @SerializedName("Recovered") var recovered: Int = 0) {

    fun getLastUpdate(): String {
        return lastUpdate.toDate().dateTimeFormat(DISPLAY_DATE_TIME_FORMAT)
    }
}
