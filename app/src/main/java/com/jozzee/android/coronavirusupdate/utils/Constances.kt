package com.jozzee.android.coronavirusupdate.utils

/**
 * Shared preferences name
 */
const val PREFERENCE_NAME = "app_preference"

const val DISPLAY_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm"

/**
 * Global Key
 */
const val KEY_DATA = "data"
const val KEY_MESSAGE = "message"
const val KEY_STATUS_CODE = "code"

/**
 * Error Code
 */
const val ERR_CODE_NO_STATUS_CODE = -1
const val ERR_CODE_NO_INTERNET_CONNECTION = 0
const val ERR_CODE_SOCKET_EXCEPTION = 481
const val ERR_CODE_NETWORK_UNREACHABLE = 482