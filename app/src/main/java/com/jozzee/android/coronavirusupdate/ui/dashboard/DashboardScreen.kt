package com.jozzee.android.coronavirusupdate.ui.dashboard


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.jozzee.android.coronavirusupdate.R
import com.jozzee.android.coronavirusupdate.datasource.spread.ConfirmedCountriesViewModel
import com.jozzee.android.coronavirusupdate.datasource.spread.ConfirmedCountriesViewModelFactory
import com.jozzee.android.coronavirusupdate.ui.component.ListItemDecorationMargin
import kotlinx.android.synthetic.main.screen_dashboard.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class DashboardScreen : Fragment() {

    private val viewModel: ConfirmedCountriesViewModel by lazy {
        ViewModelProvider(this, ConfirmedCountriesViewModelFactory())
                .get(ConfirmedCountriesViewModel::class.java)
    }

    private val adapter: ConfirmedCountryAdapter by lazy { ConfirmedCountryAdapter() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.screen_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupComponents()
    }

    private fun setupComponents() {
        confirmed_country_list.layoutManager = LinearLayoutManager(context)
        confirmed_country_list.addItemDecoration(ListItemDecorationMargin(marginAll = resources.getDimension(R.dimen.space_8dp).toInt()))
        confirmed_country_list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        //Start load data
        progress_bar.visibility = View.VISIBLE
        viewLifecycleOwner.lifecycleScope.launch(IO) {
            val list = viewModel.getConfirmedCountryList()
            viewLifecycleOwner.lifecycleScope.launch(Main) {
                progress_bar.visibility = View.GONE
                adapter.submitList(list?.toMutableList())
            }
        }
    }
}
