package com.jozzee.android.coronavirusupdate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.commit
import com.jozzee.android.coronavirusupdate.ui.dashboard.DashboardScreen

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        supportFragmentManager.commit {
            replace(R.id.main_fragment_container, DashboardScreen(), DashboardScreen::class.java.simpleName)
        }
    }
}
