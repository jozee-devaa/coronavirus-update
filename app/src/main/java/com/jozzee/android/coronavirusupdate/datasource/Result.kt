package com.jozzee.android.coronavirusupdate.datasource

import com.google.gson.annotations.SerializedName
import com.jozzee.android.coronavirusupdate.utils.ERR_CODE_NO_STATUS_CODE
import com.jozzee.android.coronavirusupdate.utils.KEY_DATA
import com.jozzee.android.coronavirusupdate.utils.KEY_MESSAGE
import com.jozzee.android.coronavirusupdate.utils.KEY_STATUS_CODE
import java.net.HttpURLConnection

data class Result<T>(@SerializedName(KEY_STATUS_CODE) var statusCode: Int = ERR_CODE_NO_STATUS_CODE,
                     @SerializedName(KEY_MESSAGE) var message: String? = null,
                     @SerializedName(KEY_DATA) var data: T? = null) {

    companion object {
        fun <T> success(data: T?) = Result(statusCode = HttpURLConnection.HTTP_OK, data = data)
        fun <T> error(statusCode: Int, message: String?) = Result<T>(statusCode, message, null)
    }

    fun isSuccessful(): Boolean = statusCode == HttpURLConnection.HTTP_OK
}