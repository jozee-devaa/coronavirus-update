package com.jozzee.android.coronavirusupdate.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jozzee.android.coronavirusupdate.R
import com.jozzee.android.coronavirusupdate.datasource.spread.ConfirmedCountryAttribute
import com.jozzee.android.coronavirusupdate.utils.displayFormat
import kotlinx.android.synthetic.main.list_item_detail.view.*

class ConfirmedCountryAdapter : ListAdapter<ConfirmedCountryAttribute, ConfirmedCountryAdapter.ViewHolder>(CountryDiffCallback()) {

    private var onItemClick: ((account: ConfirmedCountryAttribute) -> Unit)? = null

    fun setOnItemClick(block: (account: ConfirmedCountryAttribute) -> Unit) {
        onItemClick = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder.create(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CountryDiffCallback : DiffUtil.ItemCallback<ConfirmedCountryAttribute>() {
        override fun areItemsTheSame(oldItem: ConfirmedCountryAttribute, newItem: ConfirmedCountryAttribute): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: ConfirmedCountryAttribute, newItem: ConfirmedCountryAttribute): Boolean {
            return oldItem == newItem
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup) = ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_detail, parent, false))
        }

        fun bind(data: ConfirmedCountryAttribute) {
            itemView.country_label.text = data.name
            itemView.last_update_label.text = data.getLastUpdate()
            itemView.confirmed_label.text = data.confirmed.displayFormat()
            itemView.recovered_label.text = data.recovered.displayFormat()
            itemView.deaths_label.text = data.deaths.displayFormat()
        }
    }
}