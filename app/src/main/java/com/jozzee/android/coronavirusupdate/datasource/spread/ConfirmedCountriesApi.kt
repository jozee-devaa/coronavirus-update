package com.jozzee.android.coronavirusupdate.datasource.spread

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Url

interface ConfirmedCountriesApi {

    @GET
    fun getConfirmedCountriesAsync(
            @Url url: String = "https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases/FeatureServer/2/query?f=json&where=1%3D1&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Confirmed%20desc&resultOffset=0&resultRecordCount=250&cacheHint=true"
    ): Deferred<ConfirmedCountriesResult>
}