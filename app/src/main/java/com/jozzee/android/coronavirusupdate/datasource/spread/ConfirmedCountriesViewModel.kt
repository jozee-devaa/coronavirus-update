package com.jozzee.android.coronavirusupdate.datasource.spread

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class ConfirmedCountriesViewModel(private val repo: ConfirmedCountriesRepository) : ViewModel() {

    suspend fun getConfirmedCountryList(): List<ConfirmedCountryAttribute>? = withContext(IO) {
        val result = repo.getConfirmedCountries()
        if (result != null) {
            val list = ArrayList<ConfirmedCountryAttribute>()
            result.confirmedCountries?.forEach { country ->
                country.attributes?.also {
                    list.add(it)
                }
            }
            return@withContext list
        }
        return@withContext null
    }
}