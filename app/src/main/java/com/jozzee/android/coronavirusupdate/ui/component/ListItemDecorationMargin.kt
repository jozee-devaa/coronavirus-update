package com.jozzee.android.coronavirusupdate.ui.component

import android.graphics.Rect
import android.view.View
import androidx.annotation.Dimension
import androidx.recyclerview.widget.RecyclerView

class ListItemDecorationMargin() : RecyclerView.ItemDecoration() {

    @Dimension
    var marginBetweenItem: Int = 0
    @Dimension
    var marginStart: Int = 0
    @Dimension
    var marginEnd: Int = 0

    private var marginTopOfFirstItem: Boolean = true
    private var marginBottomOfLastItem: Boolean = true

    constructor(@Dimension marginBetweenItem: Int,
                marginTopOfFirstItem: Boolean = true,
                marginBottomOfLastItem: Boolean = true) : this() {
        this.marginBetweenItem = marginBetweenItem
        this.marginTopOfFirstItem = marginTopOfFirstItem
        this.marginBottomOfLastItem = marginBottomOfLastItem
    }

    constructor(@Dimension marginBetweenItem: Int,
                @Dimension marginStart: Int,
                @Dimension marginEnd: Int,
                marginTopOfFirstItem: Boolean = true,
                marginBottomOfLastItem: Boolean = true) : this() {
        this.marginBetweenItem = marginBetweenItem
        this.marginStart = marginStart
        this.marginEnd = marginEnd
        this.marginTopOfFirstItem = marginTopOfFirstItem
        this.marginBottomOfLastItem = marginBottomOfLastItem
    }

    constructor(@Dimension marginAll: Int) : this() {
        this.marginBetweenItem = marginAll
        this.marginStart = marginAll
        this.marginEnd = marginAll
        this.marginTopOfFirstItem = true
        this.marginBottomOfLastItem = true
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        /** Start Offsets **/
        if (marginStart > 0) {
            outRect.left = marginStart
        }

        /** End Offsets **/
        if (marginEnd > 0) {
            outRect.right = marginEnd
        }

        /**
         * Update margin top on first item and margin bottom on last item.
         * By default will be set margin between each a items.
         **/
        when (parent.getChildLayoutPosition(view)) {
            0 -> { //First item.
                //Top
                val outRectTop = if (marginTopOfFirstItem) marginBetweenItem else 0
                if (outRectTop > 0) {
                    outRect.top = outRectTop
                }
                //Bottom
                if (marginBetweenItem > 0) {
                    outRect.bottom = marginBetweenItem
                }
            }
            (state.itemCount - 1) -> { //Last item.
                //Bottom
                val outRectBottom = if (marginBottomOfLastItem) marginBetweenItem else 0
                if (outRectBottom > 0) {
                    outRect.bottom = outRectBottom
                }
            }
            else -> { //Second item... N item.
                if (marginBetweenItem > 0) {
                    outRect.bottom = marginBetweenItem
                }
            }
        }
    }
}