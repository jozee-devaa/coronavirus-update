package com.jozzee.android.coronavirusupdate.datasource.spread

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jozzee.android.coronavirusupdate.datasource.remote.ApiService

class ConfirmedCountriesViewModelFactory : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ConfirmedCountriesViewModel(ConfirmedCountriesRepository(ApiService.provideService(ConfirmedCountriesApi::class.java))) as T
    }
}