package com.jozzee.android.coronavirusupdate.datasource.spread

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ConfirmedCountriesRepository(private val api: ConfirmedCountriesApi) {

    suspend fun getConfirmedCountries(): ConfirmedCountriesResult? = withContext(Dispatchers.IO) {
        return@withContext try {
            api.getConfirmedCountriesAsync().await()
        } catch (error: Throwable) {
            null
        }
    }
}